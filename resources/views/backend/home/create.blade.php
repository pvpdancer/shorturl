@extends('layouts.app')

@section('style')
  <link rel="stylesheet" href="{{ asset('gijgo/css/gijgo.min.css') }}"/>
@endsection

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col">
        <div class="card">
          <div class="card-header">Add New Shorten URL</div>

          <div class="card-body">
            @if ($errors->any())
              <div class="alert alert-danger">
                <ul class="mb-0">
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif

            @if (session()->has('success'))
                <div class="alert alert-success">
                  <p class="mb-0">{{ session('success') }}</p>
                </div>
            @endif

            <form action="{{ route('backend.store') }}" method="post">
              <div class="form-group row">
                <label for="target" class="col-sm-2 col-form-label">Target URL <span class="text-danger">*</span></label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="target_url" placeholder="http://www.google.com" required>
                </div>
              </div>
              <div class="form-group row">
                <label for="inputPassword" class="col-sm-2 col-form-label">Expire Date</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control datetimepicker" name="expire_date">
                </div>
              </div>
              <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                  {{ csrf_field() }}
                  <button type="submit" class="btn btn-primary btn-save">Save</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script src="{{ asset('gijgo/js/gijgo.min.js') }}"></script>
  <script>
    $(function() {
      $('.datetimepicker').datetimepicker({
        datepicker: { minDate: new Date() },
        uiLibrary: 'bootstrap4',
        modal: true,
        footer: true,
        format: 'yyyy-mm-dd HH:MM'
      });

      setTimeout(function() {
        $('.alert').slideUp();
      }, 1500);
    });
  </script>
@endsection