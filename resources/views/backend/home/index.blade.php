@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col">
        <div class="card">
          <div class="card-header">Shortener URLs&nbsp;
            <a href="{{ route('backend.create') }}" class="btn btn-primary btn-sm">Add New</a>
          </div>

          <div class="card-body">
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul class="mb-0">
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          @if (session()->has('success'))
            <div class="alert alert-success">
              <p class="mb-0">{{ session('success') }}</p>
            </div>
          @endif

          @if (!empty($content->error))
            {{ $content->error }} !!!
          @else
            <div class="table-responsive">
              <table class="table table-bordered table-hover">
                <thead class="thead-light">
                <tr>
                  <th width="200">Url</th>
                  <th>Target</th>
                  <th width="60">Counter</th>
                  <th width="220">Expire Date</th>
                  <th width="220">Created At</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach ($content->data as $data)
                  <tr>
                    <td>
                      <a href="{{ env('APP_URL').'/'.$data->code }}" target="_blank">
                        {{ env('APP_URL').'/'.$data->code }}</a>
                    </td>
                    <td><a href="{{ $data->target }}" target="_blank">{{ substr($data->target, 0, 25) }}</a></td>
                    <td class="text-center">{{ $data->counter }}</td>
                    <td class="text-center">{{ $data->expire_at }}</td>
                    <td class="text-center">{{ $data->created_at }}</td>
                    <td>
                      <form id="del{{ $data->id }}" method="post" action="{{ route('backend.destroy', $data->id) }}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger btn-sm btn-del" data-target="#del{{ $data->id }}">
                          Delete
                        </button>
                      </form>
                    </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            @if ($content->meta->last_page > 1)
              <div class="text-center">
                @if ($content->links->prev)
                  <a href="{{ url()->current() }}?page={{ substr($content->links->prev, -1) }}"
                     class="btn btn-sm btn-outline-primary">Prev</a>
                @endif
                @if ($content->links->next)
                  <a href="{{ url()->current() }}?page={{ substr($content->links->next, -1) }}"
                     class="btn btn-sm btn-outline-primary">Next</a>
                @endif
              </div>
            @endif
          @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection