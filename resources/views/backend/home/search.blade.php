@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col">
        <div class="card">
          <div class="card-header">{{ $title }}</div>

          <div class="card-body">
            <form class="form-inline" method="get" action="{{ url()->current() }}">
              <div class="form-group mx-sm-3 mb-2">
                <input type="text" class="form-control" name="url" placeholder="URL">
              </div>
              <button type="submit" class="btn btn-primary mb-2">Search</button>
            </form>
            <hr>

            <div class="table-responsive">
              <table class="table table-bordered table-hover">
                <thead class="thead-light">
                <tr>
                  <th width="200">Url</th>
                  <th>Target</th>
                  <th width="60">Counter</th>
                  <th width="220">Expire Date</th>
                  <th width="220">Created At</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @if (!empty($content->error) || !is_object($content))
                  <tr>
                    <td colspan="6">{{ !is_object($content) ? $content : $content->error }}</td>
                  </tr>
                @else
                  @include($view)
                @endif
                </tbody>
              </table>
            </div>
            @if ($content->meta->last_page > 1)
              <div class="text-center">
                @if ($content->links->prev)
                  <a href="{{ url()->current() }}?page={{ substr($content->links->prev, -1) }}"
                     class="btn btn-sm btn-outline-primary">Prev</a>
                @endif
                @if ($content->links->next)
                  <a href="{{ url()->current() }}?page={{ substr($content->links->next, -1) }}"
                     class="btn btn-sm btn-outline-primary">Next</a>
                @endif
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection