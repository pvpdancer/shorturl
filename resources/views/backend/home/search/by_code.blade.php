<tr>
  <td>
    <a href="{{ env('APP_URL').'/'.$content->data->code }}" target="_blank">
      {{ env('APP_URL').'/'.$content->data->code }}</a>
  </td>
  <td><a href="{{ $content->data->target }}" target="_blank">{{ $content->data->target }}</a></td>
  <td class="text-center">{{ $content->data->counter }}</td>
  <td class="text-center">{{ $content->data->expire_at }}</td>
  <td class="text-center">{{ $content->data->created_at }}</td>
  <td>
    <form id="del{{ $content->data->id }}" method="post" action="{{ route('backend.destroy', $content->data->id) }}">
      {{ csrf_field() }}
      {{ method_field('DELETE') }}
      <button class="btn btn-danger btn-sm btn-del" data-target="#del{{ $content->data->id }}">
        Delete
      </button>
    </form>
  </td>
</tr>