@foreach ($content->data as $data)
  <tr>
    <td>
      <a href="{{ env('APP_URL').'/'.$data->code }}" target="_blank">
        {{ env('APP_URL').'/'.$data->code }}</a>
    </td>
    <td><a href="{{ $data->target }}" target="_blank">{{ $data->target }}</a></td>
    <td class="text-center">{{ $data->counter }}</td>
    <td class="text-center">{{ $data->expire_at }}</td>
    <td class="text-center">{{ $data->created_at }}</td>
    <td>
      <form id="del{{ $data->id }}" method="post" action="{{ route('backend.destroy', $data->id) }}">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <button class="btn btn-danger btn-sm btn-del" data-target="#del{{ $data->id }}">
          Delete
        </button>
      </form>
    </td>
  </tr>
@endforeach