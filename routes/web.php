<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::get('/', function() {
    return redirect('/backend');
});

Route::namespace('Backend')->group(function () {
    Route::get('/{code}', 'HomeController@show')->name('backend.show')->where('code', '^(?!backend).*$');

    Route::group(
        [
            'prefix' => 'backend',
            'middleware' => 'auth'
        ],
        function () {
            Route::get('/', 'HomeController@index')->name('backend.index');
            Route::get('search/short-url', 'HomeController@searchByCode')->name('backend.search.code');
            Route::get('search/long-url', 'HomeController@searchByUrl')->name('backend.search.url');
            Route::get('create', 'HomeController@create')->name('backend.create');

            Route::post('/', 'HomeController@store')->name('backend.store');

            Route::delete('/{id}', 'HomeController@destroy')->name('backend.destroy');
        }
    );
});


