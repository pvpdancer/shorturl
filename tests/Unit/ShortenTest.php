<?php

namespace Tests\Unit;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class ShortenTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        Artisan::call('db:seed');
    }

    /**
     * Test get shorten list
     */
    public function testShortenListCorrectly()
    {
        $response = $this->json('GET', '/api/shortens');
        $content = json_decode($response->getContent());

        $this->assertObjectHasAttribute('data', $content);
        $this->assertObjectHasAttribute('id', current($content->data));
        $this->assertObjectHasAttribute('code', current($content->data));
        $this->assertObjectHasAttribute('target', current($content->data));
        $this->assertObjectHasAttribute('counter', current($content->data));
        $this->assertObjectHasAttribute('expire_at', current($content->data));
    }

    /**
     * Test create shorten
     */
    public function testShortenCreateCorrectly()
    {
        $payload = [
            'target' => 'www.netflix.com',
        ];

        $response = $this->json('POST', '/api/shortens', $payload);
        $content = json_decode($response->getContent());

        $this->assertObjectHasAttribute('data', $content);
        $this->assertObjectHasAttribute('id', $content->data);
        $this->assertObjectHasAttribute('code', $content->data);
        $this->assertObjectHasAttribute('target', $content->data);
        $this->assertObjectHasAttribute('counter', $content->data);
        $this->assertObjectHasAttribute('expire_at', $content->data);
        $this->assertEquals($payload['target'], $content->data->target);
    }

    /**
     * Test create shorten with expire date
     */
    public function testShortenCreateWithExpireCorrectly()
    {
        $payload = [
            'target' => 'www.netflix.com',
            'expire' => date('Y-m-d H:i:s', strtotime("+30 minutes")),
        ];

        $response = $this->json('POST', '/api/shortens', $payload);
        $content = json_decode($response->getContent());

        $this->assertObjectHasAttribute('data', $content);
        $this->assertObjectHasAttribute('id', $content->data);
        $this->assertObjectHasAttribute('code', $content->data);
        $this->assertObjectHasAttribute('target', $content->data);
        $this->assertObjectHasAttribute('counter', $content->data);
        $this->assertObjectHasAttribute('expire_at', $content->data);
        $this->assertEquals($payload['target'], $content->data->target);
        $this->assertEquals($payload['expire'], $content->data->expire_at);
    }

    /**
     * Test get shorten
     */
    public function testShortenGetCorrectly()
    {
        $response = $this->json('GET', '/api/shortens/1');
        $content = json_decode($response->getContent());

        $this->assertObjectHasAttribute('data', $content);
        $this->assertObjectHasAttribute('id', $content->data);
        $this->assertObjectHasAttribute('code', $content->data);
        $this->assertObjectHasAttribute('target', $content->data);
        $this->assertObjectHasAttribute('counter', $content->data);
        $this->assertObjectHasAttribute('expire_at', $content->data);
    }

    /**
     * Test delete shorten
     */
    public function testShortenDeleteCorrectly()
    {
        $this->json('DELETE', '/api/shortens/1')
            ->assertStatus(410);
    }

    /**
     * Test search by shorten url
     */
    public function testShortenSearchByCodeCorrectly()
    {
        $payload = [
            'short_url' => urlencode(env('APP_URL').'/shortencode'),
        ];

        $response = $this->json('GET', '/api/shortens/search', $payload)
            ->assertStatus(200);
        $content = json_decode($response->getContent());

        $this->assertObjectHasAttribute('data', $content);
        $this->assertObjectHasAttribute('id', $content->data);
        $this->assertObjectHasAttribute('code', $content->data);
        $this->assertObjectHasAttribute('target', $content->data);
        $this->assertObjectHasAttribute('counter', $content->data);
        $this->assertObjectHasAttribute('expire_at', $content->data);
    }

    /**
     * Test search by long url
     */
    public function testShortenSearchByTargetCorrectly()
    {
        $payload = [
            'long_url' => urlencode('https:://www.google.com'),
        ];

        $response = $this->json('GET', '/api/shortens/search', $payload)
            ->assertStatus(200);
        $content = json_decode($response->getContent());

        $this->assertObjectHasAttribute('data', $content);
        $this->assertObjectHasAttribute('id', current($content->data));
        $this->assertObjectHasAttribute('code', current($content->data));
        $this->assertObjectHasAttribute('target', current($content->data));
        $this->assertObjectHasAttribute('counter', current($content->data));
        $this->assertObjectHasAttribute('expire_at', current($content->data));
    }

}
