<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        if (in_array(env('APP_ENV'), ['local', 'testing'])) {
            $this->call(LocalSeeder::class);
        }
    }
}
