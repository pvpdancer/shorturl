<?php

use Illuminate\Database\Seeder;

class LocalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shortens')->truncate();
        DB::table('shortens')->insert([
            [
                'code' => str_random(11),
                'target' => 'http://www.google.com',
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'code' => str_random(11),
                'target' => 'http://www.hotmail.com',
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'code' => str_random(11),
                'target' => 'https://www.youtube.com',
                'updated_at' => now(),
                'created_at' => now(),
            ],
            [
                'code' => 'shortencode',
                'target' => 'https://edition.cnn.com',
                'updated_at' => now(),
                'created_at' => now(),
            ]
        ]);
    }
}
