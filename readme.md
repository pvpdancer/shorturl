# Installation

### Development Environment

- Laravel
- PostGreSQL
- Laradock

### Building the Docker Environment

From the root of the repo run:

```bash
cd laradock
docker-compose up -d nginx postgres
```
This will build and run the docker development environment. 

You can access it at http://localhost

To stop it use:

```bash
docker-compose stop
```

### Command Line Access

To get command line access to the PHP container you can run

```bash
docker-compose exec workspace bash
```

### Install Dependencies

Use Composer to install the project dependencies.  
From the root of the repo run:

```bash
composer install 
```

### Set Up Migration

From the root of the repo run:
```bash
php artisan migrate
php artisan db:seed
```

### Testing
> Url: http://localhost  
E-Mail Address: demo@demo.com   
Password: demo

### Document

http://localhost/docs

### Api url

From the root of the repo find .env file

```
API_URL={{Your Host}}
```