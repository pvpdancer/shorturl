<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Shorten extends Model
{
    use SoftDeletes;

    protected $fillable = ['code', 'target', 'counter', 'expire_at'];

    /**
     * Generate unique code
     *
     * @return bool|string
     */
    public function generateCode()
    {
        // Build code length 11
        $encrypt = substr(str_shuffle(md5(now())), 0, 11);

        // Check code is use
        $isUsed = $this->getByCode($encrypt);
        if ($isUsed) {

            // Generate again if code used
            return $this->generateCode();
        }

        return $encrypt;
    }

    /**
     * Get shorten by code
     *
     * @param $code
     * @param bool $checkExpire
     * @return bool
     */
    public function getByCode($code, $checkExpire = false)
    {
        $cacheId = md5($code.'::'.$checkExpire);
        if (Cache::has($cacheId)) {
            return Cache::get($cacheId);
        }

        $query = $this->where('code', $code);
        if ($checkExpire) {
            $query = $query->where(function($q) {
                $q->whereNull('expire_at')->orWhere('expire_at', '>', now());
            });
        }

        $result = $query->first();
        if ($result) {
            Cache::put($cacheId, $result, 60);

            return $result;
        }

        return false;
    }

    /**
     * Search shorten by code or long url
     *
     * @param $url
     * @return mixed
     */
    public function searchByUrl($url)
    {
        $cacheId = md5($url.'::'.request('page'));
        if (Cache::has($cacheId)) {
            return Cache::get($cacheId);
        }

        $result = $this->where('target', 'like', '%' . $url . '%')
            ->paginate(25);

        Cache::put($cacheId, $result, 60);

        return $result;
    }
}
