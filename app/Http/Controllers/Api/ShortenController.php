<?php

namespace App\Http\Controllers\Api;

use App\Models\Shorten;
use App\Http\Resources\ShortenResource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class ShortenController extends Controller
{
    /**
     * @var Shorten
     */
    private $Shorten;

    /**
     * ShortenController constructor.
     *
     * @param Shorten $shorten
     */
    public function __construct(Shorten $shorten)
    {
        $this->Shorten = $shorten;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cacheId = md5('ShortenController::index::'.\request('page'));
        if (Cache::has($cacheId)) {
            return Cache::get($cacheId);
        }

        $result = Shorten::orderBy('id', 'desc')->paginate(25);
        if ($result->total() > 0) {
            $resource = ShortenResource::collection($result);

            Cache::put($cacheId, $resource, 60);

            return $resource;
        }

        return response()->json(['error' => 'Resource not found']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @bodyParam target string required Target url. Example: www.google.com
     * @bodyParam expire datetime Expiration date. Example: 2018-12-31 23:59:59
     *
     * @param Request $request
     * @return ShortenResource|\Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'target' => [
                'required',
                'active_url',
                'not_regex:/porn|sex|nude/'
            ],
            'expire' => 'nullable|date_format:Y-m-d H:i:s',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()]);
        }

        $result = Shorten::create([
            'code' => $this->Shorten->generateCode(),
            'target' => $request->target,
            'expire_at' => $request->expire,
        ]);

        Cache::flush();

        return new ShortenResource($result);
    }

    /**
     * Display the specified resource.
     *
     * @queryParam shorten int Shorten id.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $result = $this->Shorten->getByCode($id, true);
        if ($result) {

            // Increment counter
            $this->Shorten->whereId($result->id)->Increment('counter');

            return new ShortenResource($result);
        }

        return response()->json(['error' => 'Resource not found']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @queryParam shorten int Shorten id.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        // Get shorten by id
        $shorten = Shorten::find($id);

        if ($shorten) {
            if ($shorten->delete()) {
                Cache::flush();

                return response()->json(['data' => 'Deleted.'], 410);
            };
        }

        return response()->json(['error' => "Resource not found"]);
    }

    /**
     * Search
     *
     * Example: http://localhost/api/shortens/search?long_url=www.google.com
     *
     * @queryParam long_url string Target url
     * @queryParam short_url string Shorten url
     *
     * @response 404 {
     *  "message": "Resource not found"
     * }
     */
    public function search()
    {
        $request = request()->all();
        if (!empty($request['short_url'])) {
            $result = $this->searchByShortUrl($request['short_url']);
        } else {
            $result = $this->searchByLongUrl($request['long_url']);
        }

        if ($result) {
            return $result;
        }

        return response()->json(['error' => 'Resource not found']);
    }

    /**
     * Search by short url
     *
     * @param $url
     * @return ShortenResource|bool
     */
    private function searchByShortUrl(string $url)
    {
        $code = $this->convertUrl($url);

        $result = $this->Shorten->getByCode($code);
        if ($result) {
            return new ShortenResource($result);
        }

        return false;
    }

    /**
     * Search by long url
     *
     * @param $url
     * @return bool|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    private function searchByLongUrl(string $url)
    {
        $cacheId = md5($url.'::'.\request('page'));
        if (Cache::has($cacheId)) {
            return Cache::get($cacheId);
        }

        $url = $this->convertUrl($url, 'long');
        $result = $this->Shorten->searchByUrl($url);
        if ($result->total() > 0) {
            $resource = ShortenResource::collection($result);

            Cache::put($cacheId, $resource, 60);

            return $resource;
        }

        return false;
    }

    /**
     * Convert url
     *
     * @param $url
     * @param string $type
     * @return string
     */
    private function convertUrl(string $url, string $type = 'short'): string
    {
        $url = urldecode($url);
        switch($type) {
            case 'short':
                $url = explode('/', $url);
                return end($url);
                break;
            case 'long':
                if (strpos($url, '//')) {
                    $url = explode('//', $url);
                    return $url[1];
                }
                break;
        }

        return $url;
    }
}
