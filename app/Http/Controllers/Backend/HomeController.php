<?php

namespace App\Http\Controllers\Backend;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

use App\Http\Controllers\Controller;
use App\Http\Requests\HomePostRequest;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $template = 'backend.home.';

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Show list of shortener url
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index()
    {
        $option = [];
        if (request('page') > 1) {
            $option['query'] = ['page' => request('page')];
        }

        $response = $this->client->request(
            'GET',
            env('API_URL') . '/api/shortens',
            $option
        );
        $content = json_decode($response->getBody()->getContents());

        return view($this->template . 'index', ['content' => $content]);
    }

    /**
     * Generate form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view($this->template . 'create');
    }

    /**
     * Store new shorten url
     *
     * @param HomePostRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store(HomePostRequest $request)
    {
        $inputs = [
            'target' => $request->get('target_url'),
            'expire' => $request->get('expire_date') ? $request->get('expire_date') . ':59' : null,
        ];

        $response = $this->client->request(
            'POST',
            env('API_URL') . '/api/shortens',
            [
                'form_params' => $inputs
            ]
        );
        $content = json_decode($response->getBody()->getContents());

        if (!empty($content->error)) {
            return redirect()->route('backend.create')->withErrors($content->error);
        }

        return redirect()->route('backend.create')->with('success', 'Saved.');
    }

    /**
     * Delete shorten url
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function destroy($id)
    {
        try {
            $response = $this->client->request(
                'DELETE',
                env('API_URL') . '/api/shortens/' . $id
            );
            $content = json_decode($response->getBody()->getContents());
        }
        catch (ClientException $e) {
            $response = $e->getResponse();
            if ($response->getStatusCode() === 410) {
                return redirect()->route('backend.index')->with('success', 'Deleted.');
            }

            return redirect()->route('backend.index')->withErrors([$e->getMessage()]);
        }

        if (!empty($content->error)) {
            return redirect()->route('backend.index')->withErrors([$content->error]);
        }

        return redirect()->route('backend.index')->with('success', 'Deleted.');
    }

    /**
     * Search by short url
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function searchByCode(Request $request)
    {
        $content = 'Resource not found.';
        if (!empty($request->get('url'))) {
            $option = ['query' => ['short_url' => $request->get('url')]];

            $response = $this->client->request(
                'GET',
                env('API_URL') . '/api/shortens/search',
                $option
            );
            $content = json_decode($response->getBody()->getContents());
        }

        $dataToView = [
            'title' => 'Search by short url',
            'view' => $this->template.'.search.by_code',
            'content' => $content,
        ];

        return view($this->template.'search', $dataToView);
    }

    /**
     * Search by long url
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function searchByUrl(Request $request)
    {
        $content = 'Resource not found.';
        if (!empty($request->get('url'))) {
            $option = ['query' => ['long_url' => $request->get('url')]];

            $response = $this->client->request(
                'GET',
                env('API_URL') . '/api/shortens/search',
                $option
            );
            $content = json_decode($response->getBody()->getContents());
        }

        $dataToView = [
            'title' => 'Search by short url',
            'view' => $this->template.'.search.by_url',
            'content' => $content,
        ];

        return view($this->template.'search', $dataToView);
    }

    /**
     * Redirect to external url
     *
     * @param $code
     * @return \Illuminate\Http\RedirectResponse|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function show($code)
    {
        $response = $this->client->request(
            'GET',
            env('API_URL') . '/api/shortens/'.$code
        );
        $content = json_decode($response->getBody()->getContents());

        if (!empty($content->error)) {
            return abort(404);
        }

        return redirect()->to($content->data->target);
    }
}
