<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HomePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'target_url' => [
                'required',
                'active_url',
                'not_regex:/porn|sex|nude/'
            ],
            'expire_date' => 'nullable|date_format:Y-m-d H:i',
        ];
    }
}
