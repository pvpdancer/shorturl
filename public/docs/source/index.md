---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_aebe316a33901b007c23a878d726f5c2 -->
## Search

Example: http://localhost/api/shortens/search?long_url=www.google.com

> Example request:

```bash
curl -X GET -G "http://localhost/api/shortens/search" 
```

```javascript
const url = new URL("http://localhost/api/shortens/search");

    let params = {
            "long_url": "rZ2wyI1EPdUXAuyl",
            "short_url": "CGap49JLkkSVmqwJ",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (404):

```json
{
    "message": "Resource not found"
}
```

### HTTP Request
`GET api/shortens/search`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    long_url |  optional  | string Target url
    short_url |  optional  | string Shorten url

<!-- END_aebe316a33901b007c23a878d726f5c2 -->

<!-- START_56d34a38f268ada300ffc5ed9562a2ac -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET -G "http://localhost/api/shortens" 
```

```javascript
const url = new URL("http://localhost/api/shortens");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": [
        {
            "id": 4,
            "code": "shortencode",
            "target": "www.cnn.com",
            "counter": 0,
            "expire_at": "",
            "created_at": "2018-12-23 03:45:54",
            "updated_at": "2018-12-23 03:45:54"
        },
        {
            "id": 3,
            "code": "l2rpnu4y1at",
            "target": "www.youtube.com",
            "counter": 0,
            "expire_at": "",
            "created_at": "2018-12-23 03:45:54",
            "updated_at": "2018-12-23 03:45:54"
        },
        {
            "id": 2,
            "code": "3fPZNnBSgVu",
            "target": "www.hotmail.com",
            "counter": 0,
            "expire_at": "",
            "created_at": "2018-12-23 03:45:54",
            "updated_at": "2018-12-23 03:45:54"
        },
        {
            "id": 1,
            "code": "JUlF5SsIGj2",
            "target": "www.google.com",
            "counter": 0,
            "expire_at": "",
            "created_at": "2018-12-23 03:45:54",
            "updated_at": "2018-12-23 03:45:54"
        }
    ],
    "links": {
        "first": "http:\/\/localhost\/api\/shortens?page=1",
        "last": "http:\/\/localhost\/api\/shortens?page=1",
        "prev": null,
        "next": null
    },
    "meta": {
        "current_page": 1,
        "from": 1,
        "last_page": 1,
        "path": "http:\/\/localhost\/api\/shortens",
        "per_page": 25,
        "to": 4,
        "total": 4
    }
}
```

### HTTP Request
`GET api/shortens`


<!-- END_56d34a38f268ada300ffc5ed9562a2ac -->

<!-- START_b02dde917894ba479a8c849ad836a3d2 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://localhost/api/shortens"     -d "target"="www.google.com" \
    -d "expire"="2018-12-31 23:59:59" 
```

```javascript
const url = new URL("http://localhost/api/shortens");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

let body = JSON.stringify({
    "target": "www.google.com",
    "expire": "2018-12-31 23:59:59",
})

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`POST api/shortens`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    target | string |  required  | Target url.
    expire | datetime |  optional  | Expiration date.

<!-- END_b02dde917894ba479a8c849ad836a3d2 -->

<!-- START_1531811fff2ccd1b590e8ccc1e587162 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET -G "http://localhost/api/shortens/{shorten}" 
```

```javascript
const url = new URL("http://localhost/api/shortens/{shorten}");

    let params = {
            "shorten": "mMqFCgBaL4J8knhI",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```

> Example response (200):

```json
{
    "data": {
        "id": 1,
        "code": "JUlF5SsIGj2",
        "target": "www.google.com",
        "counter": 0,
        "expire_at": "",
        "created_at": "2018-12-23 03:45:54",
        "updated_at": "2018-12-23 03:45:54"
    }
}
```

### HTTP Request
`GET api/shortens/{shorten}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    shorten |  optional  | int Shorten id.

<!-- END_1531811fff2ccd1b590e8ccc1e587162 -->

<!-- START_6983a73c237bb978c7a3a430fed35f07 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://localhost/api/shortens/{shorten}" 
```

```javascript
const url = new URL("http://localhost/api/shortens/{shorten}");

    let params = {
            "shorten": "VdCb5W2QTpXb9Aof",
        };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


### HTTP Request
`DELETE api/shortens/{shorten}`

#### Query Parameters

Parameter | Status | Description
--------- | ------- | ------- | -----------
    shorten |  optional  | int Shorten id.

<!-- END_6983a73c237bb978c7a3a430fed35f07 -->


